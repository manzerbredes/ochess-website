var searchData=
[
  ['new_670',['New',['../classochess_1_1model_1_1_history.html#affcd3f59371afafa267ba694e612c3da',1,'ochess::model::History']]],
  ['next_671',['Next',['../classochess_1_1model_1_1_game.html#a968bfdb318cbea6b47af2197c4093521',1,'ochess::model::Game::Next()'],['../classochess_1_1model_1_1_history.html#a968b641d57cad16de47df885fda793c2',1,'ochess::model::History::Next()'],['../classochess_1_1model_1_1_history.html#ad1e54c9652a4726628b3f6734e11fa41',1,'ochess::model::History::Next(Board&lt; PPiece &gt; *board, FenState *state)']]],
  ['noduplicate_672',['NoDuplicate',['../classochess_1_1model_1_1_fen.html#a5809b9ee24d8d4970b4a80c44e72f63b',1,'ochess::model::Fen']]],
  ['notify_673',['Notify',['../classochess_1_1gui_1_1_controller.html#a7557f57bb187d11c4a4ba4d85f442bf1',1,'ochess::gui::Controller']]]
];
