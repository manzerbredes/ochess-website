var indexSectionsWithContent =
{
  0: "abcdefghijklmnopqrstuvw~",
  1: "abcdefghkmopqrsuv",
  2: "o",
  3: "abcdefghklmnopqrsuv",
  4: "abcdefghiklmnopqrstuw~",
  5: "abcdefghijlmnoprstuvw",
  6: "cfgmopruv",
  7: "cfmos",
  8: "bceikmnpqrstuw",
  9: "cfnorsu",
  10: "gotu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros",
  10: "Pages"
};

