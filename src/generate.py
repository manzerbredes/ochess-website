#!/usr/bin/env python

import os,sys
from jinja2 import Environment, PackageLoader, select_autoescape
from data import *

##### Check for arguments
if len(sys.argv) != 2:
    print("Usage: "+sys.argv[0]+" <dst>")
    exit(1)

##### Init variables
dst=sys.argv[1]

##### Generate pages
env = Environment(
    loader=PackageLoader("resources", "templates"),
    autoescape=select_autoescape(["html", "xml"])
)

for template in env.list_templates():
    with open(dst + "/" + template,"w") as file: 
        file.write(env.get_template(template).render(data))
