#!/usr/bin/env python

import sys,os,subprocess, markdown,re
from bs4 import BeautifulSoup
from datetime import datetime

##### Check for arguments
if len(sys.argv) != 2:
    print("Usage: "+sys.argv[0]+" <dst>")
    exit(1)

##### Init variables
dst=sys.argv[1]
ochess_repo="../../ochess"

##### Functions
def bash(cmd):
    result = subprocess.check_output(cmd, shell=True)
    return(result.decode("utf-8"))

def format_changelog():
    html=data["ochess"]["changelog"]
    # Add checkboxes
    html=html.replace("[ ]", '<input type="checkbox" disabled>')
    html=html.replace("[x]", '<input type="checkbox" checked disabled>')
    # Format unrelease
    html=html.replace("[unrelease]", '<b>Unrelease:</b>')
    # Format headings
    html=re.sub(r'\[([0-9]+.[0-9]+.[0-9]+)\] (.*)<',r'v\1 &nbsp;&nbsp;<small>\2</small><',html)
    # Add callout around each release
    soup=BeautifulSoup(html, 'html.parser')
    unreleaseDone=False
    for head in soup.find_all("h2"):
        callout=soup.new_tag("div")
        if not(unreleaseDone):
            callout["class"]="callout secondary"
            unreleaseDone=True
        else:
            callout["class"]="callout"
        tagInCallout=list()
        for sibling in head.find_next_siblings():
            if sibling.name == "h2": # Stop before next release
                break;
            else:
                tagInCallout.append(sibling.extract())
        head.wrap(callout)
        for tag in tagInCallout:
            callout.append(tag)
    # Remove title
    changelog=soup.find("h1")
    hr=changelog.find_next_sibling()
    changelog.extract()
    hr.extract()
    # Finally apply every changes
    data["ochess"]["changelog"]=soup
                
def init_repo_data():
    if not(os.path.exists(ochess_repo)):
        print("Could not found ochess repository: "+ochess_repo)
        exit(1)
    data["ochess"]=dict()
    data["ochess"]["nlines"]=bash("find "+ochess_repo+"/src"+" -type f -exec wc -l {} \; | awk '{total += $1} END{print total}'")
    data["ochess"]["nfiles"]=bash("find "+ochess_repo+"/src"+" -type f | wc -l")
    with open(ochess_repo+"/CHANGELOG.md", 'r') as file:
        data["ochess"]["changelog"]=markdown.markdown(file.read())
    format_changelog()

##### Initialize data
data=dict()
data["datetime"]=datetime.today().strftime("%d/%m/%Y %H:%M:%S")
data["date"]=datetime.today().strftime("%d/%m/%Y")
init_repo_data()

