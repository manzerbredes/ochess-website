var searchData=
[
  ['save_709',['Save',['../classochess_1_1_config_manager.html#aa38f76fb0c5eba9d3e2d7d2f868e08c5',1,'ochess::ConfigManager']]],
  ['savestate_710',['SaveState',['../classochess_1_1model_1_1_history.html#ae25688003475891aa805d5c561b8c5c4',1,'ochess::model::History']]],
  ['seconds_711',['Seconds',['../classochess_1_1_chrono.html#a849f4336812435943c3953104240803c',1,'ochess::Chrono']]],
  ['set_712',['set',['../classochess_1_1model_1_1_board.html#a90f5ad3dd71173a814668c285877512e',1,'ochess::model::Board']]],
  ['setboardskin_713',['SetBoardSkin',['../classochess_1_1gui_1_1_board_panel.html#a211c7d73c76245a9d00a0c7a08d8ab6b',1,'ochess::gui::BoardPanel']]],
  ['setoption_714',['setoption',['../classochess_1_1engine_1_1_u_c_i.html#a4e4dc41d2820fa5c6e20baf6486a1a90',1,'ochess::engine::UCI::setoption(std::string id)'],['../classochess_1_1engine_1_1_u_c_i.html#a5a0dbb1ad843fbf15e1a4480604e816d',1,'ochess::engine::UCI::setoption(std::string id, std::string value)']]],
  ['setpiecesskin_715',['SetPiecesSkin',['../classochess_1_1gui_1_1_board_panel.html#a63ace4e4e1a43918dcc0bea0628d4ec3',1,'ochess::gui::BoardPanel']]],
  ['setsettings_716',['SetSettings',['../classochess_1_1_configurable.html#a13fffe6c80ba2f818fabcdf9095fc7cf',1,'ochess::Configurable']]],
  ['skin_717',['Skin',['../classochess_1_1gui_1_1_skin.html#a082352f2f3c64abb70c9e6ea57e7ea44',1,'ochess::gui::Skin']]],
  ['stop_718',['stop',['../classochess_1_1engine_1_1_u_c_i.html#ac9c9115e6f0219e73669283128a2b75a',1,'ochess::engine::UCI']]],
  ['sync_719',['Sync',['../classochess_1_1engine_1_1_u_c_i_proc.html#aa7ff42a290575d98ff6a395fc276875e',1,'ochess::engine::UCIProc']]]
];
